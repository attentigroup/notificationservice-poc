import axios from 'axios';
export const ApiWrapper = {
 async get(url: string) {
   return axios.get(url).catch((err) => {
     throw new Error(`ApiWrapper get Error: ${err}`);
   });
 },
 async post(url: string, bodyData: any, config?: any) {
   return axios.post(url,  bodyData, config).catch((err) => {
     console.log(err);
     throw new Error(`ApiWrapper post Error: ${err}`);
   });
 },
 async put(url: string, bodyData: any) {
    return axios.put(url,  bodyData ).catch((err) => {
      throw new Error(`ApiWrapper put Error: ${err}`);
    });
  },
  async delete(url: string, bodyData: any) {
    return axios.delete(url,  bodyData ).catch((err) => {
      throw new Error(`ApiWrapper delete Error: ${err}`);
    });
  },
};