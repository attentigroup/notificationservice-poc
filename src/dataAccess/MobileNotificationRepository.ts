import { logger } from '../common/logger';
import { IDocumentMobileNotification, MobileNotificationDal } from './MobileNotificationDAL';
import { MobileNotificationData } from '../models/MobileNotificationData';

export class MobileNotificationRepository {

  async getMobileNotificationDataById(reqDeviceId: string): Promise<MobileNotificationData> {
    try {
        const query = { deviceId: reqDeviceId };
        const item: IDocumentMobileNotification | null = await MobileNotificationDal.findOne(query);
        return this.toNotificationData(item);
    } 
    catch (ex) {
      logger.error(`There was an uncaught error ${ex}`);
      throw new Error(ex.message);
    }
  }
  
<<<<<<< HEAD
  async add(deviceId: string, token: string): Promise<MobileNotificationData  > {
=======
  async add(deviceId: string, token: string): Promise<MobileNotificationData> {
    const a = await this.getMobileNotificationDataById(deviceId); 
    if (a){
      const aaa = deviceId;
      const query = { deviceId: aaa };
      await MobileNotificationDal.deleteOne(query);
    }
    
>>>>>>> 24da228b42833e8aec73f765def7b6a6df28eb6d
    let mobileNotificationModel = new MobileNotificationDal({ deviceId, token });
    mobileNotificationModel = await mobileNotificationModel.save();
    return this.toNotificationData(mobileNotificationModel);
  }

  toNotificationData(docNotification: IDocumentMobileNotification | null): MobileNotificationData {
    return new MobileNotificationData({
      deviceId: docNotification? docNotification.deviceId: '',
      token: docNotification? docNotification.token: ''
    });
  }

}