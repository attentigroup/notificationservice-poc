import mongoose from 'mongoose';

export interface IDocumentMobileNotification extends mongoose.Document {
    deviceId: string;
    token: string;
}
export const MobileNotificationSchema = new mongoose.Schema({
  deviceId: { type: String, required: true },
  token: { type: String, required: true },
});
export const MobileNotificationDal = mongoose.model<IDocumentMobileNotification>(
  'NotificationService',
  MobileNotificationSchema,
);
