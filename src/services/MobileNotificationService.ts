import { MobileNotificationData } from '../models/MobileNotificationData';
import { MobileNotificationRepository } from '../dataAccess/MobileNotificationRepository';
import { FireBaseConfiguration } from '../configuration/FireBaseConfig';
import { ApiWrapper } from '../utils/apiWrapper'
import { FireBaseMessage } from '../models/FireBaseMessage'
import { FireBaseData } from '../models/FireBaseData';
import { Message } from '../models/message';

export class MobileNotificationService {
  constructor(
    private readonly mobileNotificationRepository: MobileNotificationRepository = new MobileNotificationRepository(),
  ) {}
  async sendMobileNotification(deviceId: string, message: Message) {
    const device: MobileNotificationData | null = await this.mobileNotificationRepository.getMobileNotificationDataById(deviceId);
    // tslint:disable-next-line:no-console
    console.log(deviceId);
    
    const _body = message.message ? message.message : "Hello World!";
    const _title = message.title ? message.title : "Hello World!";
    const _fireBaseData = new FireBaseData({body: _body, title: _title, sound:'default'});
    const fireBaseMessage = new FireBaseMessage({to: device.token, priority: 'high', data: _fireBaseData});

    const jsonStr:string = JSON.stringify(fireBaseMessage);
    const headers = {
      headers: {Authorization: FireBaseConfiguration.AndroidAuthentication, 'Content-Type': 'application/json'}
    };
    const url:string = FireBaseConfiguration.AndroidUrl ? FireBaseConfiguration.AndroidUrl : 'https://fcm.googleapis.com/fcm/send';
    return ApiWrapper.post(url, jsonStr, headers)
     .then((res: any) => {
       return Promise.resolve(res.data);
     }) // TBD - handle errors!!
     .catch((err: any) => {
       return Promise.reject(err);
     });
  }

  async addMobileDevice(
    deviceId: string,
    token: string,
  ): Promise<MobileNotificationData> {
    return this.mobileNotificationRepository.add(deviceId, token);
  }
}
