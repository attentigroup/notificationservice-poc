import { Message } from '../models/message';
import { KafkaProvider } from '../workers/kafkaProvider';

export class MessageService {
  async getAllMessages() {
    const msg: Message[] = [
      { message: 'body', title: 'Title' },
      { message: 'body 2', title: 'Title 2' },
    ];
    return msg;
  }

  async postMessage(titleContent: string, messageContent: string): Promise<any> {
    const msgs: Message[] = [{ title: titleContent, message: messageContent }];
    const eventBroker = new KafkaProvider();
    await eventBroker.produce('Test', msgs);
  }
}
