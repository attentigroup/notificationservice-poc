import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { MobileNotificationService } from '../services/MobileNotificationService';
import { mobileNotificationRoute } from './routes/MobileNotificationController';
import { errorHandler } from './routes/errorController';

const app = express();
app.use(bodyParser.json());

export const appFactory = () => {
  app.options('*', cors());

  const mobileNotificationService: MobileNotificationService = new MobileNotificationService();
  const userRt = mobileNotificationRoute(mobileNotificationService);
  app.use('/mobileNotification', userRt);
  app.use(errorHandler);
  return app;
};
