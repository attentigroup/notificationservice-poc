import express, { Request, Response } from 'express';
import { asyncWrapper } from '../../utils/asyncWrapper';
import { MobileNotificationService } from '../../services/MobileNotificationService';

const router = express.Router();

export function mobileNotificationRoute(mobileNotificationService: MobileNotificationService) {
  router.post(
    '/addMobileDevice',
    asyncWrapper(async (req: Request, res: Response) => {
      const { deviceId, token } = req.body;
      const response = await mobileNotificationService.addMobileDevice(deviceId, token);
      res.json(response);
    }),
  );

  router.post(
    '/sendMobileNotification',
    asyncWrapper(async (req: Request, res: Response) => {
      // const { message } = req.body;
      const response = await mobileNotificationService.sendMobileNotification("123", req.body);
      res.json(response);
    }),
  );

  return router;
}
