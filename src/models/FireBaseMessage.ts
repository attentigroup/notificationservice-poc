import { FireBaseData } from './FireBaseData'

export class FireBaseMessage {
    to:string = '';
    priority:string = '';
    data!: FireBaseData | null;

    constructor(fields?: {to:string, priority:string, data:FireBaseData | null})
    {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

