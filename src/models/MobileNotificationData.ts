export class MobileNotificationData {
    deviceId: string = '';
    token: string = '';
    constructor(fields?: { deviceId?: string; token?: string }) {
      if (fields) Object.assign(this, fields);
  }
}
  