
export class FireBaseData {
    
    body:string = '';
    title:string = '';
    sound:string = 'default';

    constructor(fields?: { body?: string; title?: string; sound?: string }) {
        if (fields) Object.assign(this, fields);
    }
}