export class User {
  id: string = '';
  name: string = '';
  age: number = 0;
  constructor(fields?: { firstName?: string; lastName?: string; age?: number }) {
    if (fields) Object.assign(this, fields);
  }
}
