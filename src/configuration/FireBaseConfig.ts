import { config } from "./"

export const FireBaseConfiguration = {
    AndroidUrl: config.fireBaseUrl,
    AndroidAuthentication: config.fireBaseAuth
   };
