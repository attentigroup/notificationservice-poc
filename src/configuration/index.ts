/**
 * This module is used to collect all the configuration variables,
 * like the environment vars, in one place so they are not scattered all over the whole codebase
 */
import envVar from 'dotenv';

const result = envVar.config();
if (result.error) {
  if (
    process.env.NODE_ENV === 'production' &&
    result.error.message.indexOf('ENOENT') >= 0
  ) {
    // tslint:disable-next-line:no-console
    console.info(
      'expected this error because we are in production without a .env file',
    );
  } else {
    throw result.error;
  }
}
export const config = {
  productionMode: process.env.NODE_ENV === 'production',
  connectionString: process.env.DATABASE_CONNECTION_STRING,
  port: process.env.PORT || 5020,
  serverUrl: process.env.URL,
  fireBaseUrl: process.env.FIRE_BASE_URL,
  fireBaseAuth: process.env.FIRE_BASE_AUTH,
};
