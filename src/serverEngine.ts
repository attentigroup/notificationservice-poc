// import { KafkaProvider } from './workers/kafkaProvider';
import { Database } from './dataAccess';
import { config } from './configuration';


export class ServerEngine {
  private database: any;
  // private kafkaProvider: KafkaProvider = new KafkaProvider();

  async initServices(): Promise<void> {  
    this.database = new Database(config.connectionString as string);
    this.database.connect();    
    // await this.kafkaProvider.startConsuming('Test');
  }
  async stopServices(): Promise<void> {
    await this.database.disconnect();
    // await this.kafkaProvider.stopConsuming();
  }
}
