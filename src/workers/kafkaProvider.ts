import kafka from 'kafka-node';
import { logger } from '../common/logger';

export class KafkaProvider {
  private mainConsumer: any;
  private readonly kafkaClient: any;
  constructor() {
    const config = {
      sessionTimeout: 300,
      spinDelay: 100,
      retries: 2,
    };
    this.kafkaClient = new kafka.Client('localhost:2181', 'clientID', config);
  }
  async produce(topicInfo: string, data: any): Promise<any> {
    const messagesStr: string = JSON.stringify(data);
    const payloads = [
      { topic: topicInfo, messages: messagesStr, partition: 0 },
    ];
    const producer = new kafka.HighLevelProducer(this.kafkaClient);
    function onReady() {
      producer.send(payloads, (error: any, {}) => {
        if (error) {
          logger.error(error);
        }
        // else {
        // const formattedResult: string = result[0]; //}
      });
    }
    function onError(error: string) {
      logger.error(error);
    }
    producer.on('ready', onReady);
    producer.on('error', onError);
    return;
  }

  async startConsuming(topicContent: string): Promise<void> {
    const topics = [
      {
        topic: topicContent,
      },
    ];
    const options = {
      autoCommit: true,
      groupId: 'consumnerGrp1',
      fetchMaxWaitMs: 1000,
      fetchMaxBytes: 1024 * 1024,
    };
    this.mainConsumer = new kafka.HighLevelConsumer(
      this.kafkaClient,
      topics,
      options,
    );
    function onMessage(message: any) {
      logger.info(message);     
    }
    function onError(error: string) {
      logger.error(error);
    }
    this.mainConsumer.on('message', onMessage);
    this.mainConsumer.on('error', onError);

    return;
  }

  async stopConsuming(): Promise<any> {
    if (this.mainConsumer) {
      this.mainConsumer.close(true);
    }
  }
}
