import { config } from './configuration';
import { appFactory } from './webApi/app'; 
import { init } from './utils/signals';
import { ServerEngine } from './serverEngine';
import { logger } from './common/logger';
 
const app = appFactory();
const serverEngine = new ServerEngine();
serverEngine.initServices();

const server = app.listen(config.port, () => {
  logger.info(`Listening on *:${config.port}`);
});

const shutdown = init(async () => {
  serverEngine.stopServices();
  await server.close();
});

process.on('SIGINT', shutdown);
process.on('SIGTERM', shutdown);
