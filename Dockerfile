FROM node:carbon-jessie as attbuilder

RUN mkdir -p /build

COPY ./package.json /build/

WORKDIR /build
RUN npm install

# Bundle app source
COPY . /build

# Build app for production
RUN npm run-script build

FROM node:carbon-jessie
# user with username node is provided from the official node image
ENV user node
# Run the image as a non-root user
USER $user

# Create app directory
RUN mkdir -p /home/$user/src
WORKDIR /home/$user/src

COPY --from=attbuilder /build ./

EXPOSE 5000

ENV NODE_ENV production

CMD ["node", "./dist/server.js"]